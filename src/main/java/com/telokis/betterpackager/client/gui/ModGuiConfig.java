package com.telokis.betterpackager.client.gui;

import com.telokis.betterpackager.config.ConfigurationManager;
import com.telokis.betterpackager.reference.Reference;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.resources.I18n;
import net.minecraft.util.text.TextComponentString;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.fml.client.config.GuiConfig;
import net.minecraftforge.common.config.ConfigElement;
import net.minecraftforge.fml.client.config.GuiMessageDialog;
import net.minecraftforge.fml.client.event.ConfigChangedEvent;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.common.eventhandler.Event;

import java.util.List;

/**
 * Created by Telokis on 08/01/2017 at 05:55.
 */
public class ModGuiConfig extends GuiConfig
{
    public ModGuiConfig(GuiScreen parentScreen)
    {
        super(parentScreen,
              new ConfigElement(ConfigurationManager.config.getCategory(Configuration.CATEGORY_GENERAL))
                      .getChildElements(),
              Reference.MOD_ID,
              false,
              false,
              GuiConfig.getAbridgedConfigPath(ConfigurationManager.config.toString()));
    }
}
