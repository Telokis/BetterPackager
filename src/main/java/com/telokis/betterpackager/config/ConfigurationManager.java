package com.telokis.betterpackager.config;

import com.telokis.betterpackager.reference.Reference;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.fml.client.event.ConfigChangedEvent;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

import java.io.File;

/**
 * Created by Telokis on 08/01/2017 at 03:02.
 */
public class ConfigurationManager
{
    public static Configuration config;
    public static String val = "default";
    public static final ConfigurationManager instance = new ConfigurationManager();
    
    public static void init(File configFile)
    {
        if (config == null)
        {
            config = new Configuration(configFile);
        }
        loadConfiguration();
        FMLCommonHandler.instance().bus().register(instance);
    }
    
    public static void loadConfiguration()
    {
        // Read properties from the config
        val = config.get(Configuration.CATEGORY_GENERAL, "val", "default", "This is a test comment.").getString();
        
        if (config.hasChanged())
        {
            config.save();
        }
    }
    
    @SubscribeEvent
    public void onConfigurationChangedEvent(final ConfigChangedEvent.OnConfigChangedEvent event)
    {
        if (event.getModID().equalsIgnoreCase(Reference.MOD_ID))
        {
            loadConfiguration();
        }
    }
}
