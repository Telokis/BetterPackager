package com.telokis.betterpackager.reference;

/**
 * Created by Telokis on 08/01/2017 at 02:40.
 */
public class Reference
{
    public static final String MOD_ID             = "betterpackager";
    public static final String MOD_NAME           = "Better Packager";
    public static final String VERSION            = "@MOD_VERSION@";
    public static final String PROXY_CLIENT_CLASS = "com.telokis.betterpackager.proxy.ClientProxy";
    public static final String PROXY_SERVER_CLASS = "com.telokis.betterpackager.proxy.ServerProxy";
    public static final String GUI_FACTORY_CLASS  = "com.telokis.betterpackager.client.gui.GuiFactory";
}
