package com.telokis.betterpackager;

import com.telokis.betterpackager.config.ConfigurationManager;
import com.telokis.betterpackager.proxy.IProxy;
import com.telokis.betterpackager.reference.Reference;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

@Mod(modid = Reference.MOD_ID,
     name = Reference.MOD_NAME,
     version = Reference.VERSION,
     guiFactory = Reference.GUI_FACTORY_CLASS)
public class BetterPackager
{
    @Mod.Instance(Reference.MOD_ID)
    public static BetterPackager instance;
    
    @SidedProxy(clientSide = Reference.PROXY_CLIENT_CLASS,
                serverSide = Reference.PROXY_SERVER_CLASS)
    public static IProxy proxy;
    
    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event)
    {
        ConfigurationManager.init(event.getSuggestedConfigurationFile());
    }
    
    @Mod.EventHandler
    public void init(FMLInitializationEvent event)
    {
        
    }
    
    @Mod.EventHandler
    public void postInit(FMLPostInitializationEvent event)
    {
        
    }
}
